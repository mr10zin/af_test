package com.example.tenzin.app

//import android.R
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AFInAppEventType
import com.appsflyer.AppsFlyerLib


class MainActivity : AppCompatActivity() {
//    private val devKey = "Xae3vd3ZzAeuFFj7apc7aj";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn_click_me = findViewById(R.id.btn_click_me) as Button

        btn_click_me.setOnClickListener {
            // your code to perform when the user clicks on the button
            Toast.makeText(this@MainActivity, "You clicked me.", Toast.LENGTH_SHORT).show()
            val eventValue: MutableMap<String, Any> =
                HashMap()
            eventValue[AFInAppEventParameterName.REVENUE] = 100
            eventValue[AFInAppEventParameterName.CONTENT_TYPE] = "test"
            eventValue[AFInAppEventParameterName.CONTENT_ID] = "3434"
            eventValue[AFInAppEventParameterName.CURRENCY] = "USD"
            AppsFlyerLib.getInstance()
                .trackEvent(applicationContext, AFInAppEventType.PURCHASE, eventValue)
            Toast.makeText(this@MainActivity, "trackEvent Pinged.", Toast.LENGTH_SHORT).show()
        }



    }

}